package chelner.produs;

public abstract  class  Produs {
   private int pretDeBaza;
   private int nrCantitate;
   private int pretDeVanzare;

   public Produs( int pretDeBaza, int nrCantitate, int pretDeVanzare){
       this.pretDeBaza = pretDeBaza;
       this.nrCantitate= nrCantitate;
       this.pretDeVanzare= pretDeVanzare;
   }

    public abstract double arataPretDeBaza();

    public int getPretDeBaza() {
        return pretDeBaza;
    }

    public int getNrCantitate() {
        return nrCantitate;
    }

    public int getPretDeVanzare() {
        return pretDeVanzare;
    }

    public void setPretDeBaza(int pretDeBaza) {
        this.pretDeBaza = pretDeBaza;
    }

    public void setNrCantitate(int nrCantitate) {
        this.nrCantitate = nrCantitate;
    }

    public void setPretDeVanzare(int pretDeVanzare) {
        this.pretDeVanzare = pretDeVanzare;
    }
}
