package chelner.produs.mancare;

public class Sandwich extends Mancare {
    TipSandwich tipSandwich;


    public Sandwich(TipSandwich tipSandwich, MarimeMancare marimeMancare, boolean dietetica, int pretDeBaza, int nrCantitate, int pretDeVanzare) {
        super(marimeMancare, dietetica, pretDeBaza, nrCantitate, pretDeVanzare);
        this.tipSandwich = tipSandwich;
    }


}
