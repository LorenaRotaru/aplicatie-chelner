package chelner.produs.mancare;

import chelner.produs.Produs;

public class Mancare extends Produs {
    MarimeMancare marimeMancare;
    boolean dietetica;

    public Mancare(MarimeMancare marimeMancare,boolean dietetica, int pretDeBaza, int nrCantitate, int pretDeVanzare) {
        super(pretDeBaza, nrCantitate, pretDeVanzare);
        this.marimeMancare=marimeMancare;
        this.dietetica = dietetica;
    }

    public double arataPretDeBaza() {
        return 0;
    }
}
