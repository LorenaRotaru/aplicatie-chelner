package chelner.produs.mancare;

public class Prajitura extends  Mancare{
    TipPrajitura tipPrajitura;

    public Prajitura( TipPrajitura tipPrajitura, MarimeMancare marimeMancare, boolean dietetica, int pretDeBaza, int nrCantitate, int pretDeVanzare) {
        super(marimeMancare, dietetica, pretDeBaza, nrCantitate, pretDeVanzare);
        this.tipPrajitura= tipPrajitura;
    }
}
