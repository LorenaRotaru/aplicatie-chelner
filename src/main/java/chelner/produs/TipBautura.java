package chelner.produs;

public enum TipBautura {
    ESPRESSO, LATTE, CIOCOLATA_CALDA
}
