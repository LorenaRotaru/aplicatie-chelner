package chelner.produs;

public class Bautura extends Produs{
    TipBautura tipBautura;
    MarimeBautura marimeBautura;
    boolean alcoolica;

    public Bautura( TipBautura tipBautura,MarimeBautura marimeBautura,boolean alcoolica, int pretDeBaza, int nrCantitate, int pretDeVanzare) {
        super(pretDeBaza, nrCantitate, pretDeVanzare);
        this.tipBautura = tipBautura;
        this.marimeBautura= marimeBautura;
        this.alcoolica = alcoolica;
    }

    @Override
    public double arataPretDeBaza() {
        double pretDeBazaEspresso = 6;
        double pretDeBazaLatte = 10;
        double pretDeBazaCiocoCalda = 10;
        return arataPretDeBaza();

    }

}
